FROM balenalib/rpi-raspbian:buster

RUN apt-get update && apt-get install build-essential \
    python3-pip python3-setuptools \
    git

RUN apt-get install meson ninja-build


CMD ["bash", "start.sh"]